# Essence #
A minecraft server plugin for Spigot.
This plugin contains many essential commands and features.
Please check out the [wiki](http://mc-essence.info) for more information about Essence.

## Links ##
* [The wiki/website](http://mc-essence.info)
* Spigot plugin page
* JIRA

## Licence ##
This plugin is licensed under **MIT**.
This means that you're allowed to use the code in any way you would like as it is.
It does not mean you're allowed to distribute Essence on Spigot.
Please see the [publishing rules](https://www.spigotmc.org/wiki/plugins/) on Spigot for more information.

## API ##
If you'd like to integrate or use Essence in your plugin you can do so.
We don't really have a solid API but but there are lots of things you can do with Essence.
Please check out the [API page](http://mc-essence.info/api) on the wiki for detailed information.

## Contributing ##
This project is still in early development currently.
We really appreciate it if people help contribute by creating pull requests.
For the guidelines please see this [contributing guidelines](http://mc-essence.info/contribute_guidelines) page.